package com.bluesoft.hackathon.service;

import com.bluesoft.hackathon.model.Feedback;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class EnviaFeedBack {

    public void enviaFeedBack(Feedback feedback) {

        try {
            CloseableHttpClient client = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost("http://192.168.253.185:8065/hooks/xhaabk57pfrftfp9363kh1nk9c");

            ObjectMapper mapper = new ObjectMapper();
            String jsonInString = mapper.writeValueAsString(feedback);
            StringEntity entity = new StringEntity(jsonInString);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            CloseableHttpResponse response = null;

            response = client.execute(httpPost);
            System.out.println(response.getStatusLine().getStatusCode());
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void enviaPedidoFeedBack(List<String> nomes) {
        for (String nome : nomes) {
            try {
                Feedback feedback = new Feedback();
                feedback.setMensagem("Gostaria de dar um feedback para uns dos seus colegas? \n Para fazer isto, basta digitar /feedback @nomeDoUsuario e a sua mensagem!", false);
                feedback.setChannel("@" + nome);

                CloseableHttpClient client = HttpClients.createDefault();
                HttpPost httpPost = new HttpPost("http://192.168.253.185:8065/hooks/xhaabk57pfrftfp9363kh1nk9c");

                ObjectMapper mapper = new ObjectMapper();
                String jsonInString = mapper.writeValueAsString(feedback);
                StringEntity entity = new StringEntity(jsonInString);
                httpPost.setEntity(entity);
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                CloseableHttpResponse response = null;

                response = client.execute(httpPost);
                System.out.println(response.getStatusLine().getStatusCode());
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
