package com.bluesoft.hackathon.service;


import com.fasterxml.jackson.annotation.JsonProperty;

public class Usuario {

    @JsonProperty("login_id")
    private String login;

    @JsonProperty("password")
    private String senha;

    public Usuario(String login, String senha) {
        this.login = login;
        this.senha = senha;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
