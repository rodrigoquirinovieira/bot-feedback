package com.bluesoft.hackathon.service;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Arrays;

@Component
public class Autenticacao {

    public String gerarToken(Usuario usuario) {

        CloseableHttpClient client = null;
        String token = null;

        try {
            client = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost("http://192.168.253.185:8065/api/v3/users/login");

            ObjectMapper mapper = new ObjectMapper();
            String jsonInString = mapper.writeValueAsString(usuario);
            StringEntity entity = new StringEntity(jsonInString);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            CloseableHttpResponse response = null;

            response = client.execute(httpPost);

            token = Arrays.stream(response.getAllHeaders())
                .filter(header -> header.getName().equals("Token"))
                .map(Header::getValue)
                .findFirst()
                .get();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return token;
    }

}
