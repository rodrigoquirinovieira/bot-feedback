package com.bluesoft.hackathon.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PedidoDeFeedBackSchedular {

    @Autowired
    Autenticacao autenticacao;

    @Autowired
    ProviderUser providerUser;

    @Autowired
    EnviaFeedBack enviaFeedBack;

    @Scheduled(fixedDelayString = "${QUANTIDADE_SEGUNDOS_A_CADA_PEDIDO_FEEDBACK}")
    public void enviaPedidoDeFeedBack() {
        final String token = autenticacao.gerarToken(new Usuario("jonatas", "12345"));
        final List<String> users = providerUser.getUsers(token);
        enviaFeedBack.enviaPedidoFeedBack(users);
    }

}
