package com.bluesoft.hackathon.service;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class ProviderUser {

    @Autowired
    private Autenticacao autenticacao;


    public List<String> getUsers(String token) {

        CloseableHttpClient client = null;

        List<String> nomes = new ArrayList();

        try {
            client = HttpClients.createDefault();
            HttpGet httpGet = new HttpGet("http://192.168.253.185:8065/api/v3/users/0/10");

            httpGet.setHeader("Accept", "application/json");
            httpGet.setHeader("Content-type", "application/json");
            httpGet.setHeader("Authorization", "Bearer " + token);

            CloseableHttpResponse response = null;

            response = client.execute(httpGet);

            final String jsonUnsers = EntityUtils.toString(response.getEntity());

           nomes = nomes(jsonUnsers);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return nomes;
    }

    public List<String> nomes(String jsonCompleto) {
        int numeroUsuarios = StringUtils.countOccurrencesOf(jsonCompleto, "username");
        List<String> nomes = new ArrayList<>();

        String nome = "";

        for (int i = 0; i < numeroUsuarios; i++) {
            jsonCompleto = jsonCompleto.substring(jsonCompleto.indexOf("username") + 11, jsonCompleto.length());
            nome = jsonCompleto.substring(0, jsonCompleto.indexOf(",") - 1);
            jsonCompleto.substring(jsonCompleto.indexOf(nome) + nome.length(), jsonCompleto.length());
            nomes.add(nome);
        }

        return nomes;
    }


}
