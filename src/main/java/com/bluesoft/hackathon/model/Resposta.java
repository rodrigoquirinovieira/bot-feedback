package com.bluesoft.hackathon.model;

public class Resposta {

    private String response_type = "in_channel";
    private String text = "Rod ciclista maromba";

    public Resposta(String text) {
        this.text = text;
    }

    public String getResponse_type() {
        return response_type;
    }

    public void setResponse_type(String response_type) {
        this.response_type = response_type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
