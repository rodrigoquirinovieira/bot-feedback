package com.bluesoft.hackathon.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.Objects;


public class Feedback {

    @JsonIgnore
    private String usuarioElogiado;

    private String usuarioQueElogiou;

    @JsonProperty("icon_url")
    private String
        iconUrl =
        "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAQDQ4PEBEVDhEPEg8PDw0PDxAPDg8PFhUWFhURFhUYHSggGBolGxMVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGxAQGi0mHiU3LS0rMCsrLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAOEA4QMBEQACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAAAQIDBQYEB//EADkQAAIBAgQCBggFBAMBAAAAAAABAgMRBAUhUTFBEjJSYXGRIjNCcoGxwdEGExRioRUj4fCSovE0/8QAGgEBAAIDAQAAAAAAAAAAAAAAAAEEAgMFBv/EADURAQACAQICCAQGAgIDAQAAAAABAgMEESExBRITMkFRYXEUIjORUlOBscHRI6Hh8EJi8RX/2gAMAwEAAhEDEQA/APuIAABDdtSJmIjeR46mP1tCPS7+CKN9dG+2ON1iun8bTsxPGVNoruNPxebyhs7HH6rxx8l1oabxZnXXWjv1+zGcFZ7svVQxEZ9V/DmXMWemXuy0Xx2pzebFYmaqdGNuCepV1GoyUydSmzdjx1mvWsx/qav7fI1fE6j0Z9lj9T9TV/b5EfE6j0Oyx+qHiqv7f5HxWf0Oyx+qHi6u8CPi8/odlj9UfrKu8CPjM/8A6p7HH6q/rqu8CPjc/nU7HH6oePq9qmR8bm86nY08pQ8wq9qmR8dm86p7CnlKHmNXtQ/kj4/N51Owp5Sj+pVe1DyZH/6GbzqdhTylV5pV3h5Miekc3nX/AGn4enq2lPFr8uE5tJySdlz8EdSuprGKt7zxlVnFM2mKsEswb6kL97K9tfae5X7tsaeI70q/rKm0fAw+LzeUMuxx+rLTx3bj0f3LVG6mt/MjZrtg/DL2RaautU+ZeiYmN4aJjbhKSUAAAAAAANZmNe8uhe0Y6y73scnW5utbs45RzXMFNo63j4PDKq3pwWyOfa8zw8FmK7KXMGSY1GuDsTFpjkiYiebJCpd36suUlzNtb7zvHCWE129mR1nOTk+PRs/E2WyzktNp57MYp1Y2jzecrNqGQKsgQQlDIEMCpAggQyBVkJQQPdSklGLertotkW6zEViZ4y1zx4QSqt8/hyIm8ymKxCtzFktGo1w8uRlW8xyYzES92X4izt7MtLdmX2Z0NHn6tur4T/qf+VbPj3jfxbQ66mAAAAABEnZNkTO0bkOdqTvr2m5M8ze/W4+fF1YjYo03KSiuLGOk5LRWC1orG8t5QwkIK1rvm2rtnfxabHjjaIc6+W1pK2EhLird60YyaXFk5wVy2r4tVi8HKnrxj2tvE5Go0l8PHnHmu480X91KL0k+5mrHPNlbwYmzUzQyBUhKAIbIFSBBAhkCGQKkJQQNlgcJKok+Eecn9DoabS3zceUebTky1p7tvRwcI8rvd6s7OPS4sfKPupWzXt4rVsLCSs0l3pWaMsumx5I2mEVy2rPNo69Jwk4vl/K3OBlxzjvNZdClotG8K05WZjS20pmHRU5Xinukz01LdasS5do2nZYyQAAAADHX6kvB/I15e5PtLKnehzdzzG7qtjksLynLZJL4/wDh0ujKb2tby/n/AOKuqnhENudlSAIaurPUiYiY2kidmmrU1CdWK4JK3xs/qcLLSMeS9a8nQpabVrMvIym3KhKCBDZAqQIIEMgQyBUhKAIbIHU5d6mn7qPVaT6NfZy8vfl6Sw1gGqzqHUl4p/T6nJ6Tr3bfouaWecNYmcrdbdFg/VQ91fI9Lp/pV9ocvJ35ZjcwAAAABWaumt0zG0bxMJidpcu9NNtDyk8ODrNpkUtai91/M63RduNo9v5VNVHKW2OupgADTYx/3K3gvkjh6mf8t/0/aF/F3KvEUW9UhKGyBBAqQIZAhkCrCUECGyBVkDq8u9TT91Hq9J9Cvs5eXvy9JYawDV55LSmu9v8A3zOV0pPCse63pY4zLUHHXHUUI2hFbJI9Vjr1aRDlWne0yuZsQAAAAAObzCn0as1u7r4nmdZj6ma0fq6eG3WpC+V1ujVW0vRfx4fyZ6DL1M0evBjnr1qOhPRucAANLjPWVvh9Dhan6uT/AL5L+Pu1eJlFYVZAhkCpAggQ2BVkJQQIbIFSBDZA6zLvUU/dR6zSfQr7OXl78vSWGsA0Gb1ulVtyirfHmef6Qy9fLtHg6GnrtTfzYMHT6VSEe+78FqV9Nj7TLWrZkt1azLpj1DlgAAAAAANXnmHvFVF7OkvDfz+Zyuk8O9YyR4c/Za019p6stImcSJ2XXTZfifzKafNaSXeen0meM2OJ8fFzctOpbZ6Sy1AGkxnrK3w+SODqfq5P0/hfxdyrwsorCGQIIFSBDYEEJVIENkCpAhsgQyEuty31FL3Uet0n0K+zlZe/L0lhrYMbiFTpuXPhFbs0anPGHHNp/T3bMdOvbZzEpXbb1b1Z5ebTM7y6cRs2+RYfrVHz9GP1Z2Oi8PPJPtH8qeqv/wCMNuddUAAAAAAAVnFNNPVNWaItWLRMTyTE7TvDl8bh3Tm4vhxi90eW1OCcOSaz+jp47xeu62X4t0p39l6SXduZaTUzgvv4eKMuPr12dNCakk07p6pnpq2i0bxyc2YmJ2lYyQ0uO9bW8I/JHC1f1r/p+0L+LuVa9nPWEECCBVsCCEoIFWyBUgQ2QIISqyB1+W+ope6j1uj+hT2crL35eiUkk29EtWyxMxEby1xG7msyxn5s9OrHSK+p5rWartr8OUcnSw4upHqxYSg6k1Fc+L2XNmnBhnNeKQzveKV3l1NKmoxUVokrI9VSkUrFY5Q5dpmZ3lYyQAAAAAAAAeXMMGqsLcJLWMtn9irqtNGem3j4NuLJNJ3cxVg4ycZKzXFHmb0tS01tHGHSrMTG8PdleY/lvoy1g/8Aq9y7otbOGerbu/s05sPX4xzdFGSaTWqeqa5noomJjeHOmNmozKP95/uh/KONrq7ZveF3BPye0tYctaQyBVgQyEoIFWyBDIFWQIIShgVZA7LCLo0YX0tCN78tNT2GCOpirv4RH7OTfjadmlzXMvzPQg/Q5vtP7HE12t7X5Kd3913Bh6vGebXQi5NRSu3ol3nPrWbTFY5ysTMRG8umy3BKlDeT6z+h6bR6WMFPWebmZsvXn0ewttQAAAAAAAAAAeHMsvVVXWk1wlv3MpazR1zxvHCzdhzTSfRzVWnKEnGSs1xTPNXpalpraOLo1mLRvD15dmUqTs/Sg+Mdu9FvSa22Cdp41/7yasuGL8fFssfVjNUqsHdX6L7r8mX9ZkpkimWk8OTThrNd6y1U1ZtbNnKtG07LUTvCjZilDZCUECrZAggVbIEBKGQKkC9BJzipO0brpP8AbzM8UVm8Rbl4+zG0zEcGwzLM3V9GPow25y8fsXtZrrZvlrwr+7TiwRTjPN4acHJqMVdvRJcSlSs3tFaxxlvmYiN5dJlmXKkulLWb59lbI9HotFGCOtbvfs52bNN+EcmwL7QAAAAAAAAAAAAB5cdgYVY2ejXVkuK/wVtTpaZ67W5+EtmPLNJ4OZxmDnSlaS05SXVZ5rUabJgttaP18JdHHkreN4YqdS3PR8Vuaa2mGez11He0t9H4li3H5mMcODE2a2SCBDIFWQKtgQQIZCVSBDZAmIgejCYWdWXRgr7v2V4ssYMGTNbq0j+oYXyVpG8umwGAjRWmsnxm+Pgtkek0ujpgjhxnzc7Llm8+j1ltqAAAAAAAAAAAAAAAKVacZJxkuknyZjelbx1bRvCYmYneGhx+SSjeVL0l2H1l4bnC1XRdq/Ni4x5eP6LuPUxPCzwUJ8Yy05WfFHOpO3y2WZ84GRIhkJVZAhsCpAhshKCBVkCEru3ERG/AbjL8llK0qnoR7PtP7HW0vRd7/Nl4R5eP/Crk1MRwq39GjGEVGK6KXJHex46469WsbQo2tNp3lkM0AAAAAAAAAAAAAAAAAAA82LwNOr1lrymtJIrZ9Ljzd6OPn4tlMtqcmlxuWzp6r047parxRxdTocmLjHGF3Hnrfhylr2znt6GBUgQyEoIEMgZ8Fgp1pWjw9qT4RRY0+mvnttXl4z5NeTJFI3l0+Cy6nS6qu+29X/g9Lp9Hiwd2OPm5+TLa/N6y01AAAAAAAAAAAAAAAAAAAAAAADWY/KIzvKHoS29l/Y5mq6Ork+bHwn/UrOLUTXhbk0FelKEnGSs1yODkx2x26to2leraLRvDEzWyQQIIG0y7JpTtKp6Edval9jqaToy+T5snCP8Ac/0rZdRFeFeboaFGMIqMV0UuSPQY8dcderWNoUbWm07yyGbEAAAAAAAAAAAAAAAAAAAAAAAAAHjzPBqrTfajrF9+3gU9ZpozY/WOTbhydS3o5aotE973WzXE8vaPF0onwY2YMm3/AA7hIycqklfotKKfC/G51+itPW8zkt4clTVZJiIrDoj0CiAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcji4WdZdmpfzPJaiu1rx5S6lJ32n0eJlRtdN+G42oN9qcn/CX0PSdE12wb+cz/Tn6qfnbU6isAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcpj+viffj8zymq+pk93Sx8qtcUm91H4clfD22lJfJ/U9L0TbfT7eUz/AG52qj520OmrgAAAAAAAAAAAAAAAAAAAAAAAAAAAPBmWYql6EV06jWi5LvZQ1mtjB8tY3tLfiw9fjPJz2Jdoy6TvOb6Uu7medy2naetznmu14zw5Q8RXbXvyjMHRk7q8JW6S5r9yL2h1k6e3Huzz/tpzYuvHq62Ek0mtU1dPuPVVtFo3hzZjbgklAAAAAAAAAAAAAAAAAAAAAAAAAAAHM/iPJKtSo61KTbaSlBSaenNHF6R0OTJftcfH0/pbwZqxHVs5mWDnzcuNndvjscCYnxXuCv6WW782Ql7ssyKpXeknGCdpTbfkt2XNJor6ieHCPGWnLmjH7u8oUlCEYLhFKKvxslY9ZSkUrFY5Q5kzvO7IZIAAAAAAAAAAAAAAAAAAAAAAAAAAAAclmK/uYpbSizyesj/Nk94dPF3atcUW91X4c/8AmXvSv5np+idvh495c3VfUbQ6auAAAAAAAAAAAAAAAAAAAAAAAAAAAAAchjJXliJbzt5M8jqbda+S3q6dI2isPAU29034YnejNdmb8ml/k9H0NbfDMeU/xDn6uPniW4OuqgAAAAAAAAAAAAAAAAAAAAAAAAAAAMOLrKFOc37MW/sjVnyRjx2vPhDKletaIcfVdqUb8Ztyf+/E8faZ6kb+PF1I7zzmpsbr8MVrVJw7STXiv8M7HQ2TbJann/Cpq671iXSHolAAAAAAAAAAAAAAAAAAAAAAAAAAAABpfxBXv0KC4yalPuiuC/3Y43SubeIwx48Z9lvTV23vLQ4ud5WXCOiOFlneVykbQwmtmy4Su6dSM17Lvbdc0bcGWcWSLx4ML161Zh29KopRUlqpJNPuPZ0vF6xaOUuTMbTtKxkgAAAAAAAAAAAAAAAAAAAAAAAAAGDGYqNKDnJ2S4Lm3sjTnz0w0m92dKTedocvVrP06suvU6q2R5XJltaZyW5y6NaxwrHKHgKrcAANzkOZqH9qbtF9WT9l7eB2OjNdGP8AxZJ4eE+SpqMPW+arpT0SgAAAAAAAAAAAAAAAAAAAAAAAK1JqKbk7JcW9EjG1orG9p2hMRM8IabFZ8r9GjHpPty0ivhzORn6Wju4Y39Z5LVNNPO7V161306s/zJcorqo5OXLN562Sd5WK12jasbPHWquTu/gtita02neW2K7KGLIAAEwhtstziVJKMvTp/wDeH3R1NJ0lbDHVvxr/ALhWy6eL8Y5uhw2Lp1FeElLu5r4HoMOox5Y3pO6lalq84ZzcwAAAAAAAAAAAAAAVc0gMcsRFcwMcsbHcCjzCO4GN5nHcCjzaIGvznMFUpdFP2kzl9Lz/AIP1hZ0vfaGTPMuixym9wMUqkt35gY5Vpdp+YGKVefafmEMUsTPtPzAxyxVTty8yRjli6nbl5sD05LmFSOKoN1JW6av6T1Wxa0U7aim3m15u5L6Is2ievcpZZpHcDJHMY7gXjjY7gZI4mL5gZFUTAuAAAAAAClTgBo8wq1E9ANRWr1e8Dyzr1O8DFKtU7wKOrMCjqSAj8yRhfHTJG14iY9Uxaa8YlHTkafg9P+XX7Qz7W/4pQ5MfB6f8uv2g7W/4pRdj4PT/AJdftB2t/wAUoa7v4Hwen/Lr9oO1v+KUdHuXkPgtP+XX7Qdrf8Uo6C7K8kPg9P8Al1+0Ha385Pyl2V5IfB6f8uv2g7W/nJ+THsL/AIon4TB+Cv2g7W/nKY0kndQSa4NRV0TXTYazvFI39oROS88JmWZVJG9gsqswLxrT7wMsK9TvA9FKvV7wNngq1S+oHQYZu2oGYAAAAADAwzw8XxQGCeXwfIDFLKobAYZZPHYDHLJI7AUeRxAo8iQFXkSAq8iQEf0ICHkQBZEBP9CAf0JAWWRICyyJAXWRxAvHJI7AZI5NHYDLHKYbAZoZdBcgM8MLFcgMyVgJAAAAAAAAAAAEAAIAAQAAMAgAACQAEoAAQEgAAAAAAAf/2Q==";

    @JsonProperty("username")
    private String botName = "bluebot";

    @JsonProperty("text")
    private String mensagem;

    @JsonProperty("channel")
    private String channel = "todos";

    public String getUsuarioElogiado() {
        return usuarioElogiado;
    }

    public void setUsuarioElogiado(String usuarioElogiado) {
        usuarioElogiado = usuarioElogiado.substring(usuarioElogiado.indexOf("@"), usuarioElogiado.length());
        usuarioElogiado = usuarioElogiado.substring(0, usuarioElogiado.indexOf(" "));
        this.usuarioElogiado = usuarioElogiado;
    }

    public String getUsuarioQueElogiou() {
        return usuarioQueElogiou;
    }

    public void setUsuarioQueElogiou(String usuarioQueElogiou) {
        this.usuarioQueElogiou = "@" + usuarioQueElogiou;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem, boolean feedback) {
        if (!feedback) {
            this.mensagem = mensagem;
        } else {
            this.mensagem = usuarioQueElogiou + " enviou um feedback para " + usuarioElogiado + ":\n" + mensagem.replace(getUsuarioElogiado(), "");
        }
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    @Override
    public String toString() {
        return
            "{\"channel:\"" + channel +
            "\",username:\"" + usuarioQueElogiou +
            "\",text:\"" + mensagem + "}";
    }
}
