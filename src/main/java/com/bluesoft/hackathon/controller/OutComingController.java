package com.bluesoft.hackathon.controller;

import com.bluesoft.hackathon.model.Feedback;
import com.bluesoft.hackathon.model.Resposta;
import com.bluesoft.hackathon.service.Autenticacao;
import com.bluesoft.hackathon.service.EnviaFeedBack;
import com.bluesoft.hackathon.service.Usuario;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/outComing")
public class OutComingController {

    @Autowired
    private EnviaFeedBack enviador;

    @PostMapping
    public ResponseEntity<Resposta> outComingMessages(HttpServletRequest request){
        if(!request.getParameter("text").contains("@")) {
            return ResponseEntity.ok().body(new Resposta("Por favor cite quem você gostaria de elogiar com @"));
        }


        Feedback feedback = new Feedback();
        feedback.setUsuarioQueElogiou(request.getParameter("user_name"));
        feedback.setUsuarioElogiado(request.getParameter("text"));
        feedback.setMensagem(request.getParameter("text"), true);

        new Autenticacao().gerarToken(new Usuario("jonatas","12345"));
        enviador.enviaFeedBack(feedback);


        return ResponseEntity.ok().body(new Resposta("Elogio recebido com sucesso!"));
    }

}
